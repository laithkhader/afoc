﻿using AFOC_APP.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SAP.Middleware.Connector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AFOC_APP.Controllers
{
    public class TrackController : Controller
    {

        public ActionResult Track()
        {

            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            Session["PRs"] = null;

            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            return View();
        }



        public ActionResult Track_Filter(int ID) {

            Session["PRs"] = null;
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            var db = new AFOCDBContext();

            var FilterDate = DateTime.Now.Date;
            if (ID == 1)
            {
                FilterDate = DateTime.Now.AddDays(-2);
                FilterDate.ToShortDateString();
            }
            else if (ID == 2)
            {
                FilterDate = DateTime.Now.AddDays(-7);
                FilterDate.ToShortDateString();
            }
            else
            {
                FilterDate = DateTime.Now.AddDays(-31);
                FilterDate.ToShortDateString();
            }


            var pr = new PRViewModel();

           

            RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
            ////RfcSessionManager.BeginContext(prd);
            var DOC_ID = "";
            var format = "" + FilterDate.Day.ToString().PadLeft(2, '0') + "." + FilterDate.Month.ToString().PadLeft(2, '0') + "." + FilterDate.Year;
            try
            {
                RfcCustomDestination prd = prdc.CreateCustomDestination();
                //var user = System.Web.HttpContext.Current.User.Identity.Name;
                //prd.Password = Session["P"].ToString();
                //prd.User = user;

                RfcRepository repo = prd.Repository;
                //IRfcFunction companyBapi = repo.CreateFunction("BAPI_PR_GETDETAIL");
                IRfcFunction Requisition = repo.CreateFunction("BAPI_REQUISITION_GETITEMS");


                Requisition.SetValue("PREQ_DATE", DateTime.ParseExact(format, "dd.MM.yyyy", null));
                Requisition.SetValue("PREQ_NAME", User.Identity.Name);
                Requisition.SetValue("ASSIGNED_ITEMS", "X");
                Requisition.SetValue("CLOSED_ITEMS", "X");
                Requisition.SetValue("DELETED_ITEMS", "X");
                Requisition.SetValue("PARTIALLY_ORDERED_ITEMS", "X");
                Requisition.SetValue("OPEN_ITEMS", "X");

                Requisition.Invoke(prd);

                IRfcTable REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();

                var PREQ_ITEM = "";
                var pr3 = new PRViewModel3(); ;
                var list = new List<PRViewModel>();
                var list2 = new List<PRViewModel3>();
                int i = 1;
                foreach (var item in REQUISITION_ITEMS)
                {
                    if (DOC_ID != item.GetString("PREQ_NO"))
                    {

                    PREQ_ITEM = item.GetString("PREQ_ITEM");
                    DOC_ID = item.GetString("PREQ_NO");
                    pr = new PRViewModel();
                    pr3=new PRViewModel3();
                    pr.RepID = i++.ToString();
                    pr.DocID = DOC_ID;
                    pr.Master.DocID = item.GetString("PREQ_NO");
                    pr.Master.DeliveryDate = item.GetString("DELIV_DATE");
                    pr.REL_DATE = item.GetString("PREQ_DATE");

                    pr.Master.PlantID = item.GetString("PLANT");
                    pr.Master.MarketListPRID = item.GetString("DOC_TYPE");
                    pr.Master.PurchasingGroupID = item.GetString("PUR_GROUP");
                    //pr.Line.MaterialID = item.GetString("MATERIAL");
                    //pr.Line.Unit = item.GetString("UNIT");
                    //pr.Line.Quantity = decimal.Parse(item.GetString("QUANTITY"));
                    //pr.Line.UnitPrice = decimal.Parse(item.GetString("PREQ_PRICE"));
                    //pr.Line.TotalPrice = decimal.Parse(String.Format("{0:0.00}", pr.Line.Quantity * pr.Line.UnitPrice));
                    //pr.Line.MatDesc = item.GetString("SHORT_TEXT");





                    pr.PG_ID = pr.Master.PurchasingGroupID;
                    pr.PG_Description = db.PurchasingGroups.FirstOrDefault(m => m.name == pr.Master.PurchasingGroupID).Description;

                    pr.Plant_ID = pr.Master.PlantID;
                    pr.Plant_Description = db.Plants.FirstOrDefault(m => m.Type == pr.Master.PlantID).Description;
                    pr.Master.MarketListPRID = db.MarketListPRs.FirstOrDefault(m => m.Type == pr.Master.MarketListPRID).Description;


                    list.Add(pr);
                        pr3.DocID = item.GetString("PREQ_NO");

                        pr3.Line.MaterialID = item.GetString("MATERIAL");
                        pr3.Line.Unit = item.GetString("UNIT");
                        pr3.Line.Quantity = decimal.Parse(item.GetString("QUANTITY"));
                        pr3.Line.UnitPrice = decimal.Parse(item.GetString("C_AMT_BAPI"));
                        pr3.Line.TotalPrice = decimal.Parse(String.Format("{0:0.00}", pr3.Line.Quantity * pr3.Line.UnitPrice));
                        pr3.Line.MatDesc = item.GetString("SHORT_TEXT");
                        pr3.MAT_GRP = item.GetString("MAT_GRP");

                        list2.Add(pr3);
                    }



                }
                Session["PRs"] = list;
                Session["PRsItems"] = list2;


            }
            catch (RfcCommunicationException e)
            {
                //pr.Master.SapErrorMSG = " network problem...";
                //db.Masters.Attach(pr.Master.SapErrorMSG);
                //db.Entry(pr).State = EntityState.Modified;
                //db.SaveChanges();
                var master = new Master();
                master.SapErrorMSG = " network problem...";
                return View("ErrorPage", master);
            }
            catch (RfcLogonException e)
            {
                //res.SapErrorMSG = " user could not logon...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                var master = new Master();
                master.SapErrorMSG = " user could not logon...";
                return View("ErrorPage", master);
                // user could not logon...
            }
            catch (RfcAbapRuntimeException e)
            {
                //res.SapErrorMSG = " serious problem on ABAP system side...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                // serious problem on ABAP system side...
                var master = new Master();
                master.SapErrorMSG = " serious problem on ABAP system side...";
                return View("ErrorPage", master);
            }
            catch (RfcAbapBaseException e)
            {
                var master = new Master();
                master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                return View("ErrorPage", master);
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
            }
            catch (Exception e)
            {


                var master = new Master();
                master.SapErrorMSG = e.Message;
                return View("ErrorPage", master);
            }
            finally
            {


            }

            return View("Track", pr);

        }
        public ActionResult HierarchyBinding_Employees([DataSourceRequest] DataSourceRequest request)
        {
            List<PRViewModel> lst = null;
            var list = new List<PRViewModel>();


            lst = null;
            if (Session["PRs"] == null)
            {
                lst = new List<PRViewModel>();
                Session["PRs"] = lst;

            }


            else
            {
                list = (List<PRViewModel>)Session["PRs"];
            }

            return Json(list.ToDataSourceResult(request));
        }

        public ActionResult HierarchyBinding_Orders(string DocID, [DataSourceRequest] DataSourceRequest request)
        {
            var lst = new List<PRViewModel2>();

            RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
            ////RfcSessionManager.BeginContext(prd);
            var DOC_ID = "";
            var pr = new PRViewModel2();
            try
            {
                RfcCustomDestination prd = prdc.CreateCustomDestination();
                //var user = System.Web.HttpContext.Current.User.Identity.Name;
                //prd.Password = Session["P"].ToString();
                //prd.User = user;


                RfcRepository repo = prd.Repository;
                IRfcFunction Requisition = repo.CreateFunction("BAPI_REQUISITION_GETRELINFO");


                Requisition.SetValue("NUMBER", DocID);
                Requisition.Invoke(prd);


                IRfcTable REQUISITION_ITEMS = Requisition["RELEASE_ALREADY_POSTED"].GetTable();
                IRfcTable RELEASE_FINAL = Requisition["RELEASE_FINAL"].GetTable();
                if (REQUISITION_ITEMS.Count == 0)
                {
                    pr.DocID = DocID;
                    pr.status = "   Not Released \u26A0";

                    pr.REL_CD_TX1 = RELEASE_FINAL[0].GetString("REL_CD_TX1");

                    pr.REL_CD_TX1 += "\u26A0";


                    pr.REL_CD_TX2 += RELEASE_FINAL[0].GetString("REL_CD_TX2");
                    pr.REL_CD_TX2 += "\u26A0";
                    
                    
                        pr.REL_CD_TX3 += RELEASE_FINAL[0].GetString("REL_CD_TX3");

                        pr.REL_CD_TX3 += "\u26A0";

                   
                        pr.REL_CD_TX4 += RELEASE_FINAL[0].GetString("REL_CD_TX4");

                        pr.REL_CD_TX4 += "\u26A0";

                   
                        pr.REL_CD_TX5 += RELEASE_FINAL[0].GetString("REL_CD_TX5");

                        pr.REL_CD_TX5 += "\u26A0";

                   
                        pr.REL_CD_TX6 += RELEASE_FINAL[0].GetString("REL_CD_TX6");

                        pr.REL_CD_TX6 += "\u26A0";

                   
                        pr.REL_CD_TX7 += RELEASE_FINAL[0].GetString("REL_CD_TX7");

                        pr.REL_CD_TX7 += "\u26A0";

                     //\u2713";
                    lst.Add(pr);
                    return Json(lst.ToDataSourceResult(request));

                }
                else {
                    pr.DocID = DocID;

                    pr.status ="   Released  \u2713";

                    
                    pr.REL_CD_TX1 = REQUISITION_ITEMS[0].GetString("REL_CD_TX1");

                    pr.REL_CD_TX1 += "\u2713";

                    pr.REL_CD_TX2 = REQUISITION_ITEMS[0].GetString("REL_CD_TX2");
                    if (pr.REL_CD_TX2 == "")
                
                    {
                        pr.REL_CD_TX2 += RELEASE_FINAL[0].GetString("REL_CD_TX2");
                        pr.REL_CD_TX2 += "\u26A0";
                    }
                    else
                    {
                        pr.REL_CD_TX2 += RELEASE_FINAL[0].GetString("REL_CD_TX2");

                        pr.REL_CD_TX2 += "\u2713";

                    }
                    pr.REL_CD_TX3 = REQUISITION_ITEMS[0].GetString("REL_CD_TX3");
                    if (pr.REL_CD_TX3 == "")
                    {
                        pr.REL_CD_TX3 += RELEASE_FINAL[0].GetString("REL_CD_TX3");

                        pr.REL_CD_TX3 += "\u26A0";
                    }
                    else
                    {
                        pr.REL_CD_TX3 += RELEASE_FINAL[0].GetString("REL_CD_TX3");

                        pr.REL_CD_TX3 += "\u2713";

                    }
                    pr.REL_CD_TX4 = REQUISITION_ITEMS[0].GetString("REL_CD_TX4");
                    if (pr.REL_CD_TX4 == "")
                    {
                        pr.REL_CD_TX4 += RELEASE_FINAL[0].GetString("REL_CD_TX4");

                        pr.REL_CD_TX4 += "\u26A0";
                    }
                    else
                    {
                        pr.REL_CD_TX4 += RELEASE_FINAL[0].GetString("REL_CD_TX4");

                        pr.REL_CD_TX4 += "\u2713";

                    }
                    pr.REL_CD_TX5 = REQUISITION_ITEMS[0].GetString("REL_CD_TX5");
                    if (pr.REL_CD_TX5 == "")
                    {
                        pr.REL_CD_TX5 += RELEASE_FINAL[0].GetString("REL_CD_TX5");

                        pr.REL_CD_TX5 += "\u26A0";
                    }
                    else
                    {
                        pr.REL_CD_TX5 += RELEASE_FINAL[0].GetString("REL_CD_TX5");

                        pr.REL_CD_TX5 += "\u2713";

                    }
                    pr.REL_CD_TX6 = REQUISITION_ITEMS[0].GetString("REL_CD_TX6");
                    if (pr.REL_CD_TX6 == "")
                    {
                        pr.REL_CD_TX6 += RELEASE_FINAL[0].GetString("REL_CD_TX6");

                        pr.REL_CD_TX6 += "\u26A0";
                    }
                    else
                    {
                        pr.REL_CD_TX6 += RELEASE_FINAL[0].GetString("REL_CD_TX6");

                        pr.REL_CD_TX6 += "\u2713";

                    }
                    pr.REL_CD_TX7 = REQUISITION_ITEMS[0].GetString("REL_CD_TX7");
                    if (pr.REL_CD_TX7 == "")
                    {
                        pr.REL_CD_TX7 += RELEASE_FINAL[0].GetString("REL_CD_TX7");

                        pr.REL_CD_TX7 += "\u26A0";
                    }
                    else {
                        pr.REL_CD_TX7 += RELEASE_FINAL[0].GetString("REL_CD_TX7");

                        pr.REL_CD_TX7 += "\u2713";

                    }

                    IRfcTable i = Requisition["GENERAL_RELEASE_INFO"].GetTable();

                    pr.REL_IND_TX= i[0].GetString("REL_IND_TX");
                    lst.Add(pr);


                }




//RfcSessionManager.EndContext(prd);


            }



            catch (RfcCommunicationException e)
            {
                //pr.Master.SapErrorMSG = " network problem...";
                //db.Masters.Attach(pr.Master.SapErrorMSG);
                //db.Entry(pr).State = EntityState.Modified;
                //db.SaveChanges();
                var master = new Master();
                master.SapErrorMSG = " network problem...";
                return View("ErrorPage", master);
            }
            catch (RfcLogonException e)
            {
                //res.SapErrorMSG = " user could not logon...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                var master = new Master();
                master.SapErrorMSG = " user could not logon...";
                return View("ErrorPage", master);
                // user could not logon...
            }
            catch (RfcAbapRuntimeException e)
            {
                //res.SapErrorMSG = " serious problem on ABAP system side...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                // serious problem on ABAP system side...
                var master = new Master();
                master.SapErrorMSG = " serious problem on ABAP system side...";
                return View("ErrorPage", master);
            }
            catch (RfcAbapBaseException e)
            {
                var master = new Master();
                master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                return View("ErrorPage", master);
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
            }
            catch (Exception e)
            {


                var master = new Master();
                master.SapErrorMSG = e.Message;
                return View("ErrorPage", master);
            }
            finally
            {

            }

                return Json(lst.ToDataSourceResult(request));
        }

        public ActionResult HierarchyBinding_PRs(string DocID, [DataSourceRequest] DataSourceRequest request)
        {
            var lst = new List<PRViewModel3>();

                lst = (List<PRViewModel3>)Session["PRsItems"];
                if (lst == null)
                    return Json(lst.ToDataSourceResult(request));


                return Json(lst.Where(p => p.DocID == DocID).ToDataSourceResult(request));


            }

    }
}

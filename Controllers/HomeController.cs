﻿using AFOC_APP.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.Owin.Security;
using SAP.Middleware.Connector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;


namespace AFOC_APP.Controllers
{

    public class HomeController : Controller
    {
        private System.Object lockThis = new System.Object();
        public ActionResult Index()
        {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }

            if (Request.IsAuthenticated && User.IsInRole("PR")) {
                return RedirectToAction("Create");
            }

            if (Request.IsAuthenticated && User.IsInRole("Admin"))
            {
                return RedirectToAction("Panel","Admin");
            }


            return RedirectToAction("Login","Account");
        }
        [Authorize(Roles = "PR")]

        public ActionResult New()
        {
            Session["LE"] = null;
            Session["PRID"]=null;
            Session["PRLine"] = null;
            return RedirectToAction("Create");
        }
        [Authorize(Roles = "PR")]

        public ActionResult Create()
        {

            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }

            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");


            var master = new Master();
            AFOCDBContext Db = new AFOCDBContext();
            var PRDB = new PRDBContext();
            var lst = Db.CostCenters.Select(item => new SelectListItem() { Text = "" + item.Name.ToString().Trim() + "||" + item.CostCenterID.ToString(), Value = item.CostCenterID.ToString() }).ToList();
            ViewData["CClist"] = lst;

            var ACC = Db.AccountAssignments.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
            ViewData["ACCList"] = ACC;


            var userID = User.Identity.GetUserId();
            var MLList = PRDB.user_MarketListPRs.Where(x => x.UserID ==userID ).ToList();
            var ML = MLList.Select(item => new SelectListItem() { Text = "" + item.MasrketListPRDescription.ToString().Trim(), Value = item.MasrketListPRID.ToString() }).ToList();
            ViewData["MLList"] = ML;


            var PList = PRDB.user_Plants.Where(x => x.UserID == userID).ToList();
            var Plant = PList.Select(item => new SelectListItem() { Text = "" + item.PlantDescription.ToString().Trim(), Value = item.PlantID.ToString() }).ToList();
            ViewData["PList"] = Plant;



            var PGList = PRDB.user_PurchasingGroups.Where(x => x.UserID == userID).ToList();
            var PG = PGList.Select(item => new SelectListItem() { Text = "" + item.PurchasingGroupDescription.ToString().Trim(), Value = item.PurchasingGroupID.ToString() }).ToList();
            ViewData["PGList"] = PG;

            var mlst = Db.Materials.Select(item => new SelectListItem() { Text = "" + item.MaterialDesc.ToString().Trim() + "||" + item.MaterialID.ToString(), Value = item.MaterialID.ToString() }).ToList();
            ViewData["MMList"] = mlst;
            if (Session["PRID"] == "Error")
            {

                Session["PRID"] = null;
                master = (Master)Session["Master"];
                Session["Master"] = null;
                Session["PRID"] = null;
                return View("Create", master);
            }

            if (Session["Adopted"] != null) {
                Session["Adopted"] = null;
                master = new Master();
                master.DeliveryDateformat = DateTime.Now.Date.AddDays(1);

            }

            else if (Session["PRID"] == null) {
                master = new Master();
                Session["PRLine"] = null;
                master.DeliveryDateformat = DateTime.Now.Date.AddDays(1);
            }
             
            else
            {
                int prID=int.Parse(Session["PRID"].ToString());
                master = db.Masters.Where(s=>s.ID==prID).FirstOrDefault();
                if (master == null) {
                    master = new Master();
                    Session["PRLine"] = null;
                    master.DeliveryDateformat = DateTime.Now.Date.AddDays(1);

                }
            }


           
            return View("Create",master);
        }
        [HttpPost]
        [Authorize(Roles = "PR")]

        public ActionResult Create(Master master)
        {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");

            if (!ModelState.IsValid)

            {
                var PRDB = new PRDBContext();
                AFOCDBContext Db = new AFOCDBContext();
                var lst = Db.CostCenters.Select(item => new SelectListItem() { Text = "" + item.Name.ToString().Trim() + "||" + item.CostCenterID.ToString(), Value = item.CostCenterID.ToString() }).ToList();
                ViewData["CClist"] = lst;



                var ACC = Db.AccountAssignments.Select(item => new SelectListItem() { Text = "" + item.Description.ToString().Trim(), Value = item.Type.ToString() }).ToList();
                ViewData["ACCList"] = ACC;





                var userID = User.Identity.GetUserId();
                var MLList = PRDB.user_MarketListPRs.Where(x => x.UserID == userID).ToList();
                var ML = MLList.Select(item => new SelectListItem() { Text = "" + item.MasrketListPRDescription.ToString().Trim(), Value = item.MasrketListPRID.ToString() }).ToList();
                ViewData["MLList"] = ML;


                var PList = PRDB.user_Plants.Where(x => x.UserID == userID).ToList();
                var Plant = PList.Select(item => new SelectListItem() { Text = "" + item.PlantDescription.ToString().Trim(), Value = item.PlantID.ToString() }).ToList();
                ViewData["PList"] = Plant;



                var PGList = PRDB.user_PurchasingGroups.Where(x => x.UserID == userID).ToList();
                var PG = PGList.Select(item => new SelectListItem() { Text = "" + item.PurchasingGroupDescription.ToString().Trim(), Value = item.PurchasingGroupID.ToString() }).ToList();
                ViewData["PGList"] = PG;


                var mlst = Db.Materials.Select(item => new SelectListItem() { Text = "" + item.MaterialDesc.ToString().Trim() + "||" + item.MaterialID.ToString(), Value = item.MaterialID.ToString() }).ToList();
                ViewData["MMList"] = mlst;

                master.ErrorMSG = "VError";
                return View(master);

            }
            else if (ModelState.IsValid && Session["PRLine"] != null)
            {
                if (((List<Line>)Session["PRLine"]).Count != 0)
                {
               

                    PRDBContext db = new PRDBContext();
                    var d = new AFOCDBContext();
                    //var r = d.PurchasingGroups.FirstOrDefault(f => f.name == Request.Form["PurchasingGroupID"]).;
                    
                    var entity = new Master
                    {
                        PlantID = Request.Form["PlantID"],
                        AccountAssignmentID = Request.Form["AccountAssignmentID"],
                        PurchasingGroupID = Request.Form["PurchasingGroupID"],
                        MarketListPRID = Request.Form["MarketListPRID"],
                        Creator = System.Web.HttpContext.Current.User.Identity.Name,
                        DocID = "0",
                        HeaderNote = Request.Form["HeaderNote"],
                        
                        DeliveryDateformat = DateTime.Parse(Request.Form["DeliveryDateformat"]),
                        DeliveryDate = DateTime.Parse(Request.Form["DeliveryDateformat"]).ToString("yyyyMMdd"),
                        PurchasingTime = DateTime.Now.ToString("HH:mm:ss"),
                    };



                    var DeliveryDateFormated = entity.DeliveryDateformat.ToString("dd.MM.yyyy");
                    db.Masters.Add(entity);
                    db.SaveChanges();

                    //   var last=db.Masters.Max(p=>p.ID);
                    var lines = (List<Line>)Session["PRLine"];
                    foreach (var item in lines)
                    {
                        item.MasterID = entity.ID;
                        db.Lines.Add(item);

                    }
                    Session["PRID"] = entity.ID;




                    RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
                    try
                    {
                        RfcCustomDestination prd = prdc.CreateCustomDestination();
                        //var user = System.Web.HttpContext.Current.User.Identity.Name;
                        //prd.Password = Session["P"].ToString();
                        //prd.User = user;

                        RfcRepository repo = prd.Repository;
                        IRfcFunction companyBapi = repo.CreateFunction("BAPI_PR_CREATE");
                        IRfcFunction transaction = repo.CreateFunction("BAPI_TRANSACTION_COMMIT");
                        transaction.SetValue("WAIT", "X");

                        IRfcTable PRITEM = companyBapi["PRITEM"].GetTable();
                        IRfcTable PRITEMX = companyBapi["PRITEMX"].GetTable();
                        IRfcTable PRACCOUNT = companyBapi["PRACCOUNT"].GetTable();
                        IRfcTable PRACCOUNTX = companyBapi["PRACCOUNTX"].GetTable();
                        IRfcTable PRITEMTEXT = companyBapi["PRITEMTEXT"].GetTable();
                        IRfcTable PRHEADERTEXT = companyBapi["PRHEADERTEXT"].GetTable();
                        IRfcStructure header = companyBapi["PRHEADER"].GetStructure();
                        IRfcStructure headerx = companyBapi["PRHEADERX"].GetStructure();
                        header.SetValue("PR_TYPE", entity.MarketListPRID);
                        header.SetValue("AUTO_SOURCE", "X");
                        headerx.SetValue("PR_TYPE", "X");
                        headerx.SetValue("AUTO_SOURCE", "X");

                        var PREQ_ITEM = 0;

                        foreach (var item in lines)
                        {

                            PRITEM.Append();
                            PRITEMX.Append();
                            PRACCOUNT.Append();
                            PRACCOUNTX.Append();
                            PRITEMTEXT.Append();
                            PRHEADERTEXT.Append();

                            PRITEM.SetValue("PREQ_ITEM", ++PREQ_ITEM);
                            PRITEM.SetValue("MATERIAL", item.MaterialID);
                            PRITEM.SetValue("PREQ_NAME", User.Identity.Name);
                            PRITEM.SetValue("PUR_GROUP", entity.PurchasingGroupID);
                            PRITEM.SetValue("PLANT", entity.PlantID);
                            PRITEM.SetValue("QUANTITY", item.Quantity);
                            PRITEM.SetValue("ACCTASSCAT", entity.AccountAssignmentID);
                            PRITEM.SetValue("DELIV_DATE", DateTime.ParseExact(DeliveryDateFormated, "dd.MM.yyyy", null));

                            PRITEMX.SetValue("PREQ_ITEM", PREQ_ITEM);
                            PRITEMX.SetValue("MATERIAL", "X");
                            PRITEMX.SetValue("PREQ_NAME", "X");
                            PRITEMX.SetValue("PUR_GROUP", "X");
                            PRITEMX.SetValue("PLANT", "X");
                            PRITEMX.SetValue("QUANTITY", "X");
                            PRITEMX.SetValue("ACCTASSCAT", "X");
                            PRITEMX.SetValue("DELIV_DATE", "X");


                            PRACCOUNT.SetValue("PREQ_ITEM", PREQ_ITEM);
                            PRACCOUNT.SetValue("SERIAL_NO", 1);
                            PRACCOUNT.SetValue("QUANTITY", item.Quantity);
                            PRACCOUNT.SetValue("COSTCENTER", item.CostCenterID.PadLeft(10, '0'));
                            PRACCOUNT.SetValue("PREQ_ITEM", PREQ_ITEM);


                            PRACCOUNTX.SetValue("PREQ_ITEM", PREQ_ITEM);
                            PRACCOUNTX.SetValue("QUANTITY", "X");
                            PRACCOUNTX.SetValue("SERIAL_NO", 1);
                            PRACCOUNTX.SetValue("COSTCENTER", "X");
                            PRACCOUNTX.SetValue("PREQ_ITEM", PREQ_ITEM);


                            PRITEMTEXT.SetValue("PREQ_ITEM", PREQ_ITEM);
                            PRITEMTEXT.SetValue("TEXT_ID", "B01");
                            PRITEMTEXT.SetValue("TEXT_LINE", item.ItemText);

                            PRITEMTEXT.Append();
                            PRITEMTEXT.SetValue("PREQ_ITEM", PREQ_ITEM);
                            PRITEMTEXT.SetValue("TEXT_ID", "B04");
                            PRITEMTEXT.SetValue("TEXT_LINE", item.MaterialPoText);

                            PRHEADERTEXT.SetValue("TEXT_ID", "B01");
                            PRHEADERTEXT.SetValue("TEXT_LINE", master.HeaderNote);



                        }



                        var FilterDate = DateTime.Now.Date.AddDays(-30);

                        var format = "" + FilterDate.Day.ToString().PadLeft(2, '0') + "." + FilterDate.Month.ToString().PadLeft(2, '0') + "." + FilterDate.Year;

                        IRfcFunction Requisition = repo.CreateFunction("BAPI_REQUISITION_GETITEMS");


                        Requisition.SetValue("PREQ_DATE", DateTime.ParseExact(format, "dd.MM.yyyy", null));
                        Requisition.SetValue("PREQ_NAME", User.Identity.Name);
                        Requisition.SetValue("ASSIGNED_ITEMS", "X");
                        Requisition.SetValue("CLOSED_ITEMS", "X");
                        Requisition.SetValue("DELETED_ITEMS", "X");
                        Requisition.SetValue("PARTIALLY_ORDERED_ITEMS", "X");
                        Requisition.SetValue("OPEN_ITEMS", "X");
                        Requisition.Invoke(prd);
                        IRfcTable REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();
                        var oldN= "";
                        var newN = "";
                        if (REQUISITION_ITEMS.Count == 0)
                        {
                            oldN = "0";
                        }
                        else
                        {
                           oldN= REQUISITION_ITEMS[REQUISITION_ITEMS.Count - 1].GetString("PREQ_NO");
                        }


                            RfcTransaction trans = new RfcTransaction();
                            RfcTID tid = trans.Tid;

                            try
                            {

                                trans.AddFunction(companyBapi);
                                trans.AddFunction(transaction);
                                trans.Commit(prd);
                            }
                            catch (Exception e)
                            {
                                // Log the error, so that an admin can look at it, fix it and retry
                                // the transaction.

                            }
                            // We should execute the Confirm step in the backend only after we are 100%
                            // sure that the data has been deleted on our side, otherwise we risk a
                            // duplicate transaction!

                            prd.ConfirmTransactionID(tid); // This deletes the tid from ARFCRSTATE on
                                                           // backend side.

                        var returnID = "";

                        Requisition.SetValue("PREQ_DATE", DateTime.ParseExact(format, "dd.MM.yyyy", null));
                        Requisition.SetValue("PREQ_NAME", User.Identity.Name);
                        Requisition.SetValue("ASSIGNED_ITEMS", "X");
                        Requisition.SetValue("CLOSED_ITEMS", "X");
                        Requisition.SetValue("DELETED_ITEMS", "X");
                        Requisition.SetValue("PARTIALLY_ORDERED_ITEMS", "X");
                        Requisition.SetValue("OPEN_ITEMS", "X");
                        Requisition.Invoke(prd);
                         REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();

                        if (REQUISITION_ITEMS.Count == 0)
                        {
                            newN = "0";
                        }
                        else
                        {
                          newN=  REQUISITION_ITEMS[REQUISITION_ITEMS.Count - 1].GetString("PREQ_NO");
                        }

                        if (oldN == newN)
                        {
                            companyBapi.Invoke(prd);
                        }
                        else
                        {
                            returnID = newN;

                             entity.DocID = returnID;

                        }
                        //IRfcTable R = companyBapi["RETURN"].GetTable();


                        //var befor ="";
                        //var returnMsg = new System.Text.StringBuilder();

                        //var firstinvokeflage = false;
                        //IRfcTable REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();
                        //if (REQUISITION_ITEMS.Count == 0)
                        //{
                        //    befor = "0";
                        //    companyBapi.Invoke(prd);
                        //    firstinvokeflage = true;
                        //    R = companyBapi["RETURN"].GetTable();


                        //    foreach (var item in R )
                        //    {
                        //        if (item.GetString("TYPE") == "E")
                        //        {
                        //            returnMsg.AppendLine(item.GetString("MESSAGE"));
                        //            returnMsg.AppendLine(",");
                        //        }


                        //    }

                        //}

                        //else if(REQUISITION_ITEMS.Count!=0){

                        //    befor = REQUISITION_ITEMS[REQUISITION_ITEMS.Count -1].GetString("PREQ_NO");
                        //}
                        //var returnID = "";

                        //if (returnMsg.Length==0)
                        //{
                        //    RfcTransaction trans = new RfcTransaction();
                        //    RfcTID tid = trans.Tid;

                        //    try
                        //    {

                        //        trans.AddFunction(companyBapi);
                        //        trans.AddFunction(transaction);
                        //        trans.Commit(prd);
                        //    }
                        //    catch (Exception e)
                        //    {
                        //        // Log the error, so that an admin can look at it, fix it and retry
                        //        // the transaction.

                        //    }
                        //    // We should execute the Confirm step in the backend only after we are 100%
                        //    // sure that the data has been deleted on our side, otherwise we risk a
                        //    // duplicate transaction!

                        //    prd.ConfirmTransactionID(tid); // This deletes the tid from ARFCRSTATE on
                        //                                   // backend side.

                        //    //lock (lockThis)
                        //    //{
                        //    //RfcSessionManager.BeginContext(prd);


                        //    if (firstinvokeflage == false) { 


                        //    Requisition.SetValue("PREQ_DATE", DateTime.ParseExact(format, "dd.MM.yyyy", null));
                        //    Requisition.SetValue("PREQ_NAME", User.Identity.Name);
                        //    Requisition.SetValue("ASSIGNED_ITEMS", "X");
                        //    Requisition.SetValue("CLOSED_ITEMS", "X");
                        //    Requisition.SetValue("DELETED_ITEMS", "X");
                        //    Requisition.SetValue("PARTIALLY_ORDERED_ITEMS", "X");
                        //    Requisition.SetValue("OPEN_ITEMS", "X");

                        //    Requisition.Invoke(prd);

                        //    REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();

                        //    var after = REQUISITION_ITEMS[REQUISITION_ITEMS.Count - 1].GetString("PREQ_NO");


                        //    if (after.Equals(befor))
                        //    {

                        //        companyBapi.Invoke(prd);


                        //    }
                        //    else
                        //    {
                        //        returnID = after;

                        //        entity.DocID = returnID;
                        //    }
                        //    }
                        //    else
                        //    {
                        //        firstinvokeflage = false;
                        //    }

                        //}





                        PREQ_ITEM = 0;

                        IRfcTable RETURN = companyBapi["RETURN"].GetTable();
                      var  returnMsg = new System.Text.StringBuilder();

                        foreach (var item in RETURN)
                        {
                            if (item.GetString("TYPE") == "E")
                            {
                                returnMsg.AppendLine(item.GetString("MESSAGE"));
                                returnMsg.AppendLine(",");


                            }

                            if (item.GetString("TYPE") == "S")
                            {
                                if (item[7].GetValue() != null && item[7].GetValue().ToString().Trim() != "")
                                {
                                    

                                    Requisition.SetValue("PREQ_DATE", DateTime.ParseExact(format, "dd.MM.yyyy", null));
                                    Requisition.SetValue("PREQ_NAME", User.Identity.Name);
                                    Requisition.SetValue("ASSIGNED_ITEMS", "X");
                                    Requisition.SetValue("CLOSED_ITEMS", "X");
                                    Requisition.SetValue("DELETED_ITEMS", "X");
                                    Requisition.SetValue("PARTIALLY_ORDERED_ITEMS", "X");
                                    Requisition.SetValue("OPEN_ITEMS", "X");


                                    Requisition.Invoke(prd);


                                    REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();

                                    var st = REQUISITION_ITEMS[REQUISITION_ITEMS.Count - 1].GetString("PREQ_NO");


                                    returnID = st;
                                    
                                    entity.DocID = returnID;


                                }

                            }
                        }


                        entity.SapErrorMSG = returnMsg.ToString();

                        if (entity.SapErrorMSG != "" || entity.ID == 0)
                        {

                            var itemToRemove = db.Masters.SingleOrDefault(x => x.ID == entity.ID); //returns a single item.
                            master = entity;
                            if (itemToRemove != null)
                            {
                                db.Masters.Remove(itemToRemove);
                                db.SaveChanges();

                                Session["LE"] = "true";
                                Session["Master"] = master;
                                Session["PRID"]="Error";

                                //master.ErrorMSG = "VError";
                                
                                return RedirectToAction("Create", "Home", master);

                            }

                            //m.SapErrorMSG =entity.SapErrorMSG;
                            //m.ID = entity.ID;
                            //db.Masters.Add(m);

                        }
                      

                       

                        db.SaveChanges();
                        master = entity;



                    }
                    catch (RfcCommunicationException e)
                    {
                        
                         master = new Master();
                        master.SapErrorMSG = e.Message;// " network problem...";
                        return View("ErrorPage", master);
                    }
                    catch (RfcLogonException e)
                    {
                        

                         master = new Master();
                        master.SapErrorMSG = e.Message;// " user could not logon...";
                        return View("ErrorPage", master);
                        // user could not logon...
                    }
                    catch (RfcAbapRuntimeException e)
                    {
                       
                         master = new Master();
                        master.SapErrorMSG = e.Message;//" serious problem on ABAP system side...";
                        return View("ErrorPage", master);
                    }
                    catch (RfcAbapBaseException e)
                    {
                       master = new Master();
                        master.SapErrorMSG = e.Message;// " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                        return View("ErrorPage", master);
                        // The function module returned an ABAP exception, an ABAP message
                        // or an ABAP class-based exception...
                    }
                    catch (Exception e) {
                        


                        master = new Master();
                        master.SapErrorMSG = e.Message;
                        return View("ErrorPage", master);
                    }

                    
                
                    finally
                    {
                    }





                }
            }
            return RedirectToAction("Create", "Home",master);
        }

    

        private PRDBContext db = new PRDBContext();

        public ActionResult Lines_Read([DataSourceRequest]DataSourceRequest request,int MasterID)
        {
            List<Line> lst = null;
            var list = new List<Line>();

            if (Session["LE"]=="true") {
                Session["LE"] = "";
                MasterID = 0;

            }
            if (MasterID == 0)
            { 
                if (Session["PRLine"] == null)
                {
                    lst = new List<Line>();
                    Session["PRLine"] = lst;
                }
                else
                {
                    lst = (List<Line>)Session["PRLine"];
                  
                }
                foreach (var item in lst)
                {

                    if (item.CostCenterID==null) {

                        item.CostCenterID = "";
                    }

                    list.Add(item);
                }
            }
            else
            {
                PRDBContext entities = new PRDBContext();

                IList<Line> result = new List<Line>();

                 list = entities.Lines.Where(p => p.MasterID == MasterID).ToList().Select(row => new Line
                {
                    ID = row.ID,
                    MaterialID = row.MaterialID,
                    CostCenterID = row.CostCenterID,
                    Quantity = row.Quantity,
                    UnitPrice = row.UnitPrice,
                    Unit = row.Unit,
                    ItemText = row.ItemText,
                    MaterialPoText = row.MaterialPoText,
                    TotalPrice = row.TotalPrice,



                }

                 ).ToList();

                lst = null;
                if (Session["PRLine"] == null)
                {
                    lst = new List<Line>();
                    Session["PRLine"] = lst;


                }
                else
                {
                    lst = (List<Line>)Session["PRLine"];
                    lst.Clear();
                }
                foreach (var item in list)
                {
                    lst.Add(item);
                }
            }
            
            return Json(list.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Create([DataSourceRequest]DataSourceRequest request, Line line)
        {
            AFOCDBContext Db = new AFOCDBContext();
            var cc = Db.CostCenters.SingleOrDefault(a => a.CostCenterID == line.CostCenterID);
            var M = Db.Materials.SingleOrDefault(a => a.MaterialID == line.MaterialID);
            Session["FirstCostCenter"] = cc.CostCenterID;
            if (ModelState.IsValid&&line!=null)
            {
                IList<Line> list = null;
                if (Session["PRLine"] == null )//|| ((List<Line>)Session["PRLine"]).Count == 0)
                {
                    list = new List<Line>();
                   
                    line.ID = 1;
                    line.MatDesc = M.MaterialDesc;
                    line.Unit = M.UnitType;
                    line.UnitPrice = M.UnitPrice;
                    line.CostCenterName = cc.Name;
                    line.CostCenterDescription = cc.Description;
                    list.Add(line);
                    Session["PRLine"] = list;
                }
                else
                {
                    list = (List<Line>)Session["PRLine"];
                    var first = list.OrderByDescending(e => e.ID).FirstOrDefault();
                    var id = (first != null) ? first.ID : 0;
                    line.ID = id + 1;
                    
                    line.MatDesc = M.MaterialDesc;
                    line.Unit = M.UnitType;
                    line.UnitPrice = M.UnitPrice;
                    line.CostCenterName = cc.Name;
                    line.CostCenterDescription = cc.Description;
                    list.Add(line);
                    Session["PRLine"] = list;
                }
            }

            return Json(new[] { line }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Update([DataSourceRequest]DataSourceRequest request, Line line)
        {

            AFOCDBContext Db = new AFOCDBContext();
            var cc = Db.CostCenters.SingleOrDefault(a => a.CostCenterID == line.CostCenterID);
            var M = Db.Materials.SingleOrDefault(a => a.MaterialID == line.MaterialID);

            IList<Line> list = null;
            if (ModelState.IsValid)
            {
                list = (List<Line>)Session["PRLine"];
                line.MaterialID = M.MaterialID;
                line.MatDesc = M.MaterialDesc;
                line.CostCenterName = cc.Name;
                line.CostCenterDescription = cc.Description;
                foreach (var item in list)
                {
                    if (line.ID == item.ID) {
                        item.ItemText = line.ItemText;

                        item.MaterialID = line.MaterialID;
                        item.MatDesc = line.MatDesc;
                        item.CostCenterName = line.CostCenterName;
                        item.CostCenterDescription = line.CostCenterDescription;

                        item.Quantity = line.Quantity;
                        item.Unit = line.Unit;
                        item.UnitPrice = line.UnitPrice;
                        item.TotalPrice = line.TotalPrice;
                        item.CostCenterID = line.CostCenterID;
                        item.MaterialPoText = line.MaterialPoText;
                        break;
                    }

                }
                //Session["PRLine"] = list;
                //var entity = new Line
                //{
                //    ID = line.ID,
                //    MatDesc = line.MatDesc,
                //    Quantity = line.Quantity,
                //    Unit = line.Unit,
                //    UnitPrice = line.UnitPrice,
                //    TotalPrice = line.TotalPrice,
                //    CostCenterName = line.CostCenterName,
                //    CostCenterDescription = line.CostCenterDescription,
                //    ItemText = line.ItemText,
                //    MaterialPoText = line.MaterialPoText,
                //};
            

                //db.Lines.Attach(entity);
                //db.Entry(entity).State = EntityState.Modified;
                //db.SaveChanges();
            }

            return Json(new[] { line }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Destroy([DataSourceRequest]DataSourceRequest request, Line line)

        {

            if (line != null)
            {
                List<Line> lst = null;
                if (Session["PRLine"] == null)
                {
                    lst = new List<Line>();
                    Session["line"] = lst;


                }
                else
                {
                    lst = (List<Line>)Session["PRLine"];
                    var pppp = lst.SingleOrDefault(a => a.ID == line.ID);
                    if (pppp != default(Line))
                        lst.Remove(pppp);

                }
            }

            return Json(new[] { line }.ToDataSourceResult(request, ModelState));
        }

        public JsonResult GetMaterial(string ID) {
            AFOCDBContext Db = new AFOCDBContext();
            var data = Db.Materials.FirstOrDefault(m => m.MaterialID == ID);
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        public JsonResult CopyFirstCostCenter(string ID)
        {
            if (Session["FirstCostCenter"]==null) {
                Session["FirstCostCenter"] = "";
            }

           var data = Session["FirstCostCenter"].ToString();
            return Json(data, JsonRequestBehavior.AllowGet);

        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
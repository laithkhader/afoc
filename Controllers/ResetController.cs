﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using AFOC_APP.Models;
using SAP.Middleware.Connector;
using Microsoft.AspNet.Identity;

namespace AFOC_APP.Controllers
{
    [Authorize(Roles = "PR")]

    public class ResetController : Controller
    {
        public ActionResult Reset()
        {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            var db = new AFOCDBContext();

            var pr = new PRViewModel();
            var pr2 = new PRViewModel2();



            RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
            ////RfcSessionManager.BeginContext(prd);
            var d = new ApplicationDbContext();

            var rel_user = d.Users.First(f => f.UserName == User.Identity.Name);

            try
            {
                RfcCustomDestination prd = prdc.CreateCustomDestination();
                //var user = System.Web.HttpContext.Current.User.Identity.Name;
                //prd.Password = Session["P"].ToString();
                //prd.User = user;

                var list = new List<PRViewModel>();
                var list2 = new List<PRViewModel2>();

                var PRdb = new PRDBContext();
                var id = User.Identity.GetUserId();

                var Release_Codes = PRdb.user_ReleaseCodes.Where(f => f.UserID == id).ToList();

                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("BAPI_PR_GETDETAIL");
                foreach (var R in Release_Codes)
                {
                    IRfcFunction Requisition = repo.CreateFunction("BAPI_REQUISITION_GETITEMSREL");
                    Requisition.SetValue("REL_GROUP", "R1");
                    Requisition.SetValue("REL_CODE", R.ReleaseCodeID);
                    Requisition.SetValue("ITEMS_FOR_RELEASE", "");


                    Requisition.Invoke(prd);

                    IRfcTable REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();
   
                    var PREQ_ITEM = "";
                    var DOC_ID = "";

                    int i = 1;
                    foreach (var item in REQUISITION_ITEMS)
                    {
                        if (item.GetString("PREQ_NAME") == User.Identity.Name)
                        {


                            if (DOC_ID == item.GetString("PREQ_NO"))
                            {
                                pr2 = new PRViewModel2();
                                DOC_ID = item.GetString("PREQ_NO");
                                pr2.DocID = item.GetString("PREQ_NO");

                                pr2.Line.MaterialID = item.GetString("MATERIAL");
                                pr2.Line.Unit = item.GetString("UNIT");
                                pr2.Line.Quantity = decimal.Parse(item.GetString("QUANTITY"));
                                pr2.Line.UnitPrice = decimal.Parse(item.GetString("C_AMT_BAPI"));
                                pr2.Line.TotalPrice = decimal.Parse(String.Format("{0:0.00}", pr2.Line.Quantity * pr2.Line.UnitPrice));
                                pr2.Line.MatDesc = item.GetString("SHORT_TEXT");


                                companyBapi.SetValue("NUMBER", DOC_ID);
                                companyBapi.SetValue("ACCOUNT_ASSIGNMENT", "X");
                                companyBapi.Invoke(prd);
                                IRfcTable PRACCOUNT = companyBapi["PRACCOUNT"].GetTable();
                                foreach (var f in PRACCOUNT)
                                {
                                    if (f.GetString("PREQ_ITEM") == PREQ_ITEM)
                                    {
                                        pr2.Line.CostCenterID = f.GetString("COSTCENTER");

                                    }

                                }


                                pr2.MAT_GRP = item.GetString("MAT_GRP");


                                pr2.Line.CostCenterName = db.CostCenters.FirstOrDefault(m => m.CostCenterID == pr2.Line.CostCenterID).Name;
                                pr2.Line.CostCenterDescription = db.CostCenters.FirstOrDefault(m => m.CostCenterID == pr2.Line.CostCenterID).Description;
                                pr2.REL__Code = R.ReleaseCodeID;
                                list2.Add(pr2);


                            }

                            else
                            {
                                pr = new PRViewModel();



                                PREQ_ITEM = item.GetString("PREQ_ITEM");
                                DOC_ID = item.GetString("PREQ_NO");
                                pr.RepID = i++.ToString();
                                pr.DocID = item.GetString("PREQ_NO");

                                pr.Master.DocID = item.GetString("PREQ_NO");
                                pr.Master.DeliveryDate = item.GetString("DELIV_DATE");
                                pr.REL_DATE = item.GetString("PREQ_DATE");

                                pr.Master.PlantID = item.GetString("PLANT");
                                pr.Master.MarketListPRID = item.GetString("DOC_TYPE");
                                pr.Master.PurchasingGroupID = item.GetString("PUR_GROUP");


                                pr.PG_ID = pr.Master.PurchasingGroupID;
                                pr.PG_Description = db.PurchasingGroups.FirstOrDefault(m => m.name == pr.Master.PurchasingGroupID).Description;

                                pr.Plant_ID = pr.Master.PlantID;
                                pr.Plant_Description = db.Plants.FirstOrDefault(m => m.Type == pr.Master.PlantID).Description;
                                pr.REL_Code = R.ReleaseCodeID;
                                list.Add(pr);
                                pr2 = new PRViewModel2();
                                DOC_ID = item.GetString("PREQ_NO");
                                pr2.DocID = item.GetString("PREQ_NO");

                                pr2.Line.MaterialID = item.GetString("MATERIAL");
                                pr2.Line.Unit = item.GetString("UNIT");
                                pr2.Line.Quantity = decimal.Parse(item.GetString("QUANTITY"));
                                pr2.Line.UnitPrice = decimal.Parse(item.GetString("C_AMT_BAPI"));
                                pr2.Line.TotalPrice = decimal.Parse(String.Format("{0:0.00}", pr2.Line.Quantity * pr2.Line.UnitPrice));
                                pr2.Line.MatDesc = item.GetString("SHORT_TEXT");


                                companyBapi.SetValue("NUMBER", DOC_ID);
                                companyBapi.SetValue("ACCOUNT_ASSIGNMENT", "X");
                                companyBapi.Invoke(prd);
                                IRfcTable PRACCOUNT = companyBapi["PRACCOUNT"].GetTable();
                                foreach (var f in PRACCOUNT)
                                {
                                    if (f.GetString("PREQ_ITEM") == PREQ_ITEM)
                                    {
                                        pr2.Line.CostCenterID = f.GetString("COSTCENTER");

                                    }

                                }


                                pr2.MAT_GRP = item.GetString("MAT_GRP");


                                pr2.Line.CostCenterName = db.CostCenters.FirstOrDefault(m => m.CostCenterID == pr2.Line.CostCenterID).Name;
                                pr2.Line.CostCenterDescription = db.CostCenters.FirstOrDefault(m => m.CostCenterID == pr2.Line.CostCenterID).Description;
                                pr2.REL__Code = R.ReleaseCodeID ;
                                list2.Add(pr2);


                            }


                        }

                    }

                }

                Session["PRs"] = list;
                Session["PRsItems"] = list2;

                //list= list.GroupBy(u => u.Master.DocID)
                //      .Select(grp => grp.ToList())
                //      .ToList();



//RfcSessionManager.EndContext(prd);

            }
            catch (RfcCommunicationException e)
            {
                //pr.Master.SapErrorMSG = " network problem...";
                //db.Masters.Attach(pr.Master.SapErrorMSG);
                //db.Entry(pr).State = EntityState.Modified;
                //db.SaveChanges();
                var master = new Master();
                master.SapErrorMSG = " network problem...";
                return View("ErrorPage", master);
            }
            catch (RfcLogonException e)
            {
                //res.SapErrorMSG = " user could not logon...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                var master = new Master();
                master.SapErrorMSG = " user could not logon...";
                return View("ErrorPage", master);
                // user could not logon...
            }
            catch (RfcAbapRuntimeException e)
            {
                //res.SapErrorMSG = " serious problem on ABAP system side...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                // serious problem on ABAP system side...
                var master = new Master();
                master.SapErrorMSG = " serious problem on ABAP system side...";
                return View("ErrorPage", master);
            }
            catch (RfcAbapBaseException e)
            {
                var master = new Master();
                master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                return View("ErrorPage", master);
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
            }
            catch (Exception e)
            {


                var master = new Master();
                master.SapErrorMSG = e.Message;
                return View("ErrorPage", master);
            }
            finally
            {

            }

            return View("Reset", pr);
        }
        public ActionResult ReleaseRes([DataSourceRequest]DataSourceRequest request, PRViewModel PR)

        {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            if (PR != null)
            {
                List<PRViewModel> lst = null;
                if (Session["PRs"] == null)
                {
                    lst = new List<PRViewModel>();
                    Session["line"] = lst;


                }
                else
                {
                    lst = (List<PRViewModel>)Session["PRs"];




                    RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
                    ////RfcSessionManager.BeginContext(prd);
                    var d = new ApplicationDbContext();

                    var rel_user = d.Users.First(f => f.UserName == User.Identity.Name);


                    try
                    {


                        RfcCustomDestination prd = prdc.CreateCustomDestination();
                        //var user = System.Web.HttpContext.Current.User.Identity.Name;
                        //prd.Password = Session["P"].ToString();
                        //prd.User = user;
                        RfcRepository repo = prd.Repository;

                        IRfcFunction Requisition = repo.CreateFunction("BAPI_REQUISITION_RESET_REL_GEN");
                        Requisition.SetValue("NUMBER", PR.Master.DocID);
                        Requisition.SetValue("REL_CODE", PR.REL_Code);
                        Requisition.Invoke(prd);



                        IRfcTable RETURN = Requisition["RETURN"].GetTable();
                        var returnMsg = new System.Text.StringBuilder();

                        var returnID = "";
                        foreach (var item in RETURN)
                        {
                            if (item.GetString("TYPE") == "E")
                            {
                                returnMsg.AppendLine(item.GetString("MESSAGE"));
                                returnMsg.AppendLine(",");


                            }

                        }
        //RfcSessionManager.EndContext(prd);

                    }
                    catch (RfcCommunicationException e)
                    {
                        //pr.Master.SapErrorMSG = " network problem...";
                        //db.Masters.Attach(pr.Master.SapErrorMSG);
                        //db.Entry(pr).State = EntityState.Modified;
                        //db.SaveChanges();
                        var master = new Master();
                        master.SapErrorMSG = " network problem...";
                        return View("ErrorPage", master);
                    }
                    catch (RfcLogonException e)
                    {
                        //res.SapErrorMSG = " user could not logon...";
                        //db.Masters.Attach(res);
                        //db.Entry(res).State = EntityState.Modified;
                        //db.SaveChanges();
                        //return View("Create");

                        var master = new Master();
                        master.SapErrorMSG = " user could not logon...";
                        return View("ErrorPage", master);
                        // user could not logon...
                    }
                    catch (RfcAbapRuntimeException e)
                    {
                        //res.SapErrorMSG = " serious problem on ABAP system side...";
                        //db.Masters.Attach(res);
                        //db.Entry(res).State = EntityState.Modified;
                        //db.SaveChanges();
                        //return View("Create");

                        // serious problem on ABAP system side...
                        var master = new Master();
                        master.SapErrorMSG = " serious problem on ABAP system side...";
                        return View("ErrorPage", master);
                    }
                    catch (RfcAbapBaseException e)
                    {
                        var master = new Master();
                        master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                        return View("ErrorPage", master);
                        // The function module returned an ABAP exception, an ABAP message
                        // or an ABAP class-based exception...
                    }
                    catch (Exception e)
                    {


                        var master = new Master();
                        master.SapErrorMSG = e.Message;
                        return View("ErrorPage", master);
                    }



                    var REQ = lst.Where(a => a.Master.DocID == PR.Master.DocID).ToList();
                    if (REQ != null)
                    {
                        foreach (var item in REQ)
                        {
                            lst.Remove(item);

                        }
                    }

                  


                }
            }

            return Json(new[] { PR }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Masters_Read([DataSourceRequest]DataSourceRequest request)
        {
            List<PRViewModel> lst = null;
            var list = new List<PRViewModel>();


            lst = null;
            if (Session["PRs"] == null)
            {
                lst = new List<PRViewModel>();
                Session["PRs"] = lst;

            }


            else
            {
                list = (List<PRViewModel>)Session["PRs"];
            }

            return Json(list.ToDataSourceResult(request));

        }

        public ActionResult HierarchyBinding_Employees([DataSourceRequest] DataSourceRequest request)
        {
            List<PRViewModel> lst = null;
            var list = new List<PRViewModel>();


            lst = null;
            if (Session["PRs"] == null)
            {
                lst = new List<PRViewModel>();
                Session["PRs"] = lst;

            }


            else
            {
                list = (List<PRViewModel>)Session["PRs"];
            }

            return Json(list.ToDataSourceResult(request));
        }

        public ActionResult HierarchyBinding_Orders(string DocID, [DataSourceRequest] DataSourceRequest request)
        {
            var lst = new List<PRViewModel2>();
            lst = (List<PRViewModel2>)Session["PRsItems"];
             if (lst==null)
                return Json(lst.ToDataSourceResult(request));

            return Json(lst.Where(p => p.DocID == DocID).ToDataSourceResult(request));
        }

    }
}
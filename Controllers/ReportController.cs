﻿﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using AFOC_APP.Models;
using SAP.Middleware.Connector;

namespace AFOC_APP.Views.Report
{
    [Authorize(Roles = "PR")]

    public class ReportController : Controller
    {
        private PRDBContext db = new PRDBContext();

        public ActionResult Report()
        {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            Session["PRs"] = null;
            return View();
        }
        public ActionResult Filter(int ID)
        {
            if (Session["P"] == null)
            {

                return RedirectToAction("LogOff", "Account");

            }
            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account");
            var db = new AFOCDBContext();

            var FilterDate = DateTime.Now.Date;
            if (ID==1)
            {
                 FilterDate = DateTime.Now.AddDays(-2);
                FilterDate.ToShortDateString();
            }
            else if (ID==2)
            {
                 FilterDate = DateTime.Now.AddDays(-7);
                FilterDate.ToShortDateString();
            }
            else
            {
                 FilterDate = DateTime.Now.AddDays(-31);
                FilterDate.ToShortDateString();
            }


            var pr = new PRViewModel();

           

            RfcDestination prdc = RfcDestinationManager.GetDestination("AFOCH");
            //////RfcSessionManager.BeginContext(prd);
            var DOC_ID="";
            var format =""+ FilterDate.Day.ToString().PadLeft(2, '0') + "." + FilterDate.Month.ToString().PadLeft(2,'0') + "." + FilterDate.Year;
            try
            {
                RfcCustomDestination prd = prdc.CreateCustomDestination();
                //var user = System.Web.HttpContext.Current.User.Identity.Name;
                //prd.Password = Session["P"].ToString();
                //prd.User = user;
                RfcRepository repo = prd.Repository;
                IRfcFunction companyBapi = repo.CreateFunction("BAPI_PR_GETDETAIL");
                IRfcFunction Requisition = repo.CreateFunction("BAPI_REQUISITION_GETITEMS");


                Requisition.SetValue("PREQ_DATE", DateTime.ParseExact(format, "dd.MM.yyyy", null));
                Requisition.SetValue("PREQ_NAME", User.Identity.Name);
                Requisition.SetValue("ASSIGNED_ITEMS", "X");
                Requisition.SetValue("CLOSED_ITEMS", "X");
                Requisition.SetValue("DELETED_ITEMS", "X");
                Requisition.SetValue("PARTIALLY_ORDERED_ITEMS", "X");
                Requisition.SetValue("OPEN_ITEMS","X");

                Requisition.Invoke(prd);

                IRfcTable REQUISITION_ITEMS = Requisition["REQUISITION_ITEMS"].GetTable();


                var PREQ_ITEM = "";

                var list = new List<PRViewModel>();

                int i = 0;
                int j = 0;
                foreach (var item in REQUISITION_ITEMS)
                {
                    if (DOC_ID!= item.GetString("PREQ_NO")) {
                        j = 0;
                    }

                        PREQ_ITEM = item.GetString("PREQ_ITEM");
                        DOC_ID = item.GetString("PREQ_NO");
                        pr = new PRViewModel();
                        pr.RepID = i++.ToString();
                        pr.Master.DocID = item.GetString("PREQ_NO");
                        var date = DateTime.Parse( item.GetString("DELIV_DATE"));
                    var foo= "" + date.Day.ToString().PadLeft(2, '0') + "." + date.Month.ToString().PadLeft(2, '0') + "." + date.Year;
                    pr.Master.DeliveryDate= foo;

                     date = DateTime.Parse(item.GetString("PREQ_DATE"));
                     foo = "" + date.Day.ToString().PadLeft(2, '0') + "." + date.Month.ToString().PadLeft(2, '0') + "." + date.Year;
                    pr.REL_DATE =foo;


                    

                        pr.Master.PlantID = item.GetString("PLANT");
                        pr.Master.MarketListPRID = item.GetString("DOC_TYPE");
                        pr.Master.PurchasingGroupID = item.GetString("PUR_GROUP");
                        pr.Line.MaterialID = item.GetString("MATERIAL");
                        pr.Line.Unit = item.GetString("UNIT");
                        pr.Line.Quantity = decimal.Parse(item.GetString("QUANTITY"));
                        pr.Line.UnitPrice = decimal.Parse(item.GetString("C_AMT_BAPI"));
                        pr.Line.TotalPrice = decimal.Parse(String.Format("{0:0.00}", pr.Line.Quantity * pr.Line.UnitPrice)) ;
                        pr.Line.MatDesc = item.GetString("SHORT_TEXT");




                        companyBapi.SetValue("NUMBER", DOC_ID);
                        companyBapi.SetValue("ACCOUNT_ASSIGNMENT", "X");
                        companyBapi.Invoke(prd);
                        IRfcTable PRACCOUNT = companyBapi["PRACCOUNT"].GetTable();

                        IRfcTable PRITEMS = companyBapi["PRITEM"].GetTable();


                    if (PRITEMS[j].GetString("DELETE_IND") != "")
                    {
                        pr.Line.DELETE_IND = "DEL";
                    }

                    foreach (var f in PRACCOUNT)
                    {
                        if (f.GetString("PREQ_ITEM") ==PREQ_ITEM) {
                            pr.Line.CostCenterID = f.GetString("COSTCENTER");

                        }

                    }
                   

                        pr.MAT_GRP = item.GetString("MAT_GRP");
                    if (pr.Line.CostCenterID != null) {

                        pr.Line.CostCenterName = db.CostCenters.FirstOrDefault(m => m.CostCenterID == pr.Line.CostCenterID).Name;
                        pr.Line.CostCenterDescription = db.CostCenters.FirstOrDefault(m => m.CostCenterID == pr.Line.CostCenterID).Description;

                    }
                      

                        pr.PG_ID = pr.Master.PurchasingGroupID;
                        pr.PG_Description = db.PurchasingGroups.FirstOrDefault(m => m.name == pr.Master.PurchasingGroupID).Description;

                        pr.Plant_ID = pr.Master.PlantID;
                        pr.Plant_Description = db.Plants.FirstOrDefault(m => m.Type == pr.Master.PlantID).Description;
                        pr.Master.MarketListPRID=db.MarketListPRs.FirstOrDefault(m => m.Type == pr.Master.MarketListPRID).Description;

                    list.Add(pr);



                    j++;


                }
                Session["PRs"]= list;

//RfcSessionManager.EndContext(prd);

                //    lineList.Add(line);
                //    lineListO.Add(line);
                //    res.PurchasingGroupID = item.GetString("PUR_GROUP");
                //    res.MarketListPRID = item.GetString("DOC_TYPE");
                //    res.PlantID = item.GetString("PLANT");
                //    res.AccountAssignmentID = item.GetString("ACCTASSCAT");
                //    res.DeliveryDateformat = DateTime.Parse(item.GetString("DELIV_DATE"));
                //    //db.Lines.Attach(line);
                //    //db.Entry(line).State = EntityState.Modified;
                //    //db.SaveChanges();

                //}



                //Session["PRLine"] = lineList;
                //Session["PRLineO"] = lineListO;



                //IRfcTable RETURN = companyBapi["RETURN"].GetTable();
                //var returnMsg = new System.Text.StringBuilder();

                //foreach (var item in RETURN)
                //{
                //    if (item.GetString("TYPE") == "E")
                //    {
                //        returnMsg.AppendLine(item.GetString("MESSAGE"));
                //        returnMsg.AppendLine("");


                //    }

                //    //if (item.GetString("TYPE") == "S")
                //    //{
                //    //    if (item[7].GetValue() != null && item[7].GetValue().ToString().Trim() != "")
                //    //    {
                //    //        returnID = item[7].GetValue().ToString();
                //    //        entity.DocID = returnID;

                //    //    }

                //    //}
                //}
                //res.SapErrorMSG = returnMsg.ToString();

                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //RfcSessionManager.EndContext(prd);

            }
            catch (RfcCommunicationException e)
            {
                //pr.Master.SapErrorMSG = " network problem...";
                //db.Masters.Attach(pr.Master.SapErrorMSG);
                //db.Entry(pr).State = EntityState.Modified;
                //db.SaveChanges();
                var master = new Master();
                master.SapErrorMSG = " network problem...";
                return View("ErrorPage", master);
            }
            catch (RfcLogonException e)
            {
                //res.SapErrorMSG = " user could not logon...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                var master = new Master();
                master.SapErrorMSG = " user could not logon...";
                return View("ErrorPage", master);
                // user could not logon...
            }
            catch (RfcAbapRuntimeException e)
            {
                //res.SapErrorMSG = " serious problem on ABAP system side...";
                //db.Masters.Attach(res);
                //db.Entry(res).State = EntityState.Modified;
                //db.SaveChanges();
                //return View("Create");

                // serious problem on ABAP system side...
                var master = new Master();
                master.SapErrorMSG = " serious problem on ABAP system side...";
                return View("ErrorPage", master);
            }
            catch (RfcAbapBaseException e)
            {
                var master = new Master();
                master.SapErrorMSG = " The function module returned an ABAP exception, an ABAP message or an ABAP class-based exception...";
                return View("ErrorPage", master);
                // The function module returned an ABAP exception, an ABAP message
                // or an ABAP class-based exception...
            }
            catch (Exception e)
            {


                var master = new Master();
                master.SapErrorMSG = e.Message;
                return View("ErrorPage", master);
            }
            finally
            {

            }

            return View("Report",pr);
        }



        public ActionResult Masters_Read([DataSourceRequest]DataSourceRequest request)
        {
            List<PRViewModel> lst = null;
            var list = new List<PRViewModel>();

           
                lst = null;
            if (Session["PRs"] == null)
            {
                lst = new List<PRViewModel>();
                Session["PRs"] = lst;

            }


            else
            {
                list = (List<PRViewModel>)Session["PRs"];
            }

            return Json(list.ToDataSourceResult(request));

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Adopt([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<PRViewModel> updated)
        {
            var i = db.Masters.Any() ? db.Masters.Max(x => x.ID) : 0;
            var last = 0;
            if (i == 0)
            {
                last = 0;
            }
            else {
               last = db.Masters.Max(p => p.ID);

            }



            if (!Request.IsAuthenticated)
                return RedirectToAction("Login", "Account"); 
            var lst = new List<PRViewModel>();
            var line = new List<Line>();

            if (updated != null)
            {

                int lineID = 1;
                foreach (PRViewModel PR in updated)
                {
                    if (PR.Checked == true) {
                        PR.Line.ID = lineID++;
                        PR.Master.ID =last ;
                        line.Add(PR.Line);
                    }

                   

                }
                Session["PRLine"] = line;
                Session["Adopted"] = "Adopted";
                return JavaScript("window.location = '"+System.Configuration.ConfigurationManager.AppSettings["JSAdptRdrct"].ToString()+"'");
          
            }
            Session["Adopted"] = null;

            return Json(ModelState.ToDataSourceResult());
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}

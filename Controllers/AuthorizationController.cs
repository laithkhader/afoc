﻿using AFOC_APP.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AFOC_APP.Controllers
{
    public class AuthorizationController : Controller
    {

        public ActionResult Lines_Read([DataSourceRequest]DataSourceRequest request)
        {
            List<User_ReleaseCodes> lst = null;
            var list = new List<User_ReleaseCodes>();


            lst = null;
            if (Session["UREL"] == null)
            {
                lst = new List<User_ReleaseCodes>();
                Session["UREL"] = lst;

            }


            else
            {
                list = (List<User_ReleaseCodes>)Session["UREL"];
            }


            return Json(list.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Create([DataSourceRequest]DataSourceRequest request, User_ReleaseCodes UR)
        {
            var PRDB = new PRDBContext();

            var list = new List<User_ReleaseCodes>();
            var check = PRDB.user_ReleaseCodes.FirstOrDefault(x => x.ReleaseCodeID == UR.ReleaseCodeID);
            if (check == null)
            {

                UR.UserID = Session["UserId"].ToString();
                var re = PRDB.releaseCodes.Where(x => x.Codes == UR.ReleaseCodeID).FirstOrDefault();
                UR.ReleaseCodeDescription = re.Discription;
                var u = new User_ReleaseCodes();
                if (((List<User_ReleaseCodes>)Session["UREL"]).Count == 0)//|| ((List<Line>)Session["PRLine"]).Count == 0)
                {
                    UR.ID = 1;
                    list.Add(UR);

                    Session["UREL"] = list;
                }
                else
                {
                    list = (List<User_ReleaseCodes>)Session["UREL"];
                    var first = list.OrderByDescending(e => e.ID).FirstOrDefault();
                    var id = (first != null) ? first.ID : 0;
                    UR.ID = id + 1;


                    list.Add(UR);


                    Session["UREL"] = list;
                }

                var UREL = new User_ReleaseCodes
                {

                    ID = UR.ID,
                    ReleaseCodeDescription = UR.ReleaseCodeDescription,
                    ReleaseCodeID = UR.ReleaseCodeID,
                    UserID = UR.UserID,
                };
                PRDB.user_ReleaseCodes.Add(UREL);
                PRDB.SaveChanges();

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Update([DataSourceRequest]DataSourceRequest request, User_ReleaseCodes UR)
        {
            var PRDB = new PRDBContext();
            
            var lst = (List<User_ReleaseCodes>)Session["UREL"];
            var check = lst.First(x => x.ReleaseCodeID == UR.ReleaseCodeID);
            if (check == null)
            {
                UR.UserID = Session["UserId"].ToString();
            var re = PRDB.releaseCodes.Where(x => x.Codes == UR.ReleaseCodeID).FirstOrDefault();
            UR.ReleaseCodeDescription = re.Discription;
            var i = lst.Where(x => x.ID == UR.ID).First();

            
                lst.Remove(i);
                lst.Add(UR);
                var UREL = new User_ReleaseCodes
                {

                    ID = UR.ID,
                    ReleaseCodeDescription = UR.ReleaseCodeDescription,
                    ReleaseCodeID = UR.ReleaseCodeID,
                    UserID = UR.UserID,
                };
                PRDB.user_ReleaseCodes.Attach(UREL);
                PRDB.Entry(UREL).State = EntityState.Modified;
                PRDB.SaveChanges();
                Session["UREL"] = lst;

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }







            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Destroy([DataSourceRequest]DataSourceRequest request, User_ReleaseCodes UR)
        {
            var PRDB = new PRDBContext();



            if (UR != null)
            {
                List<User_ReleaseCodes> lst = null;
                if (Session["UREL"] == null)
                {
                    lst = new List<User_ReleaseCodes>();
                    Session["UREL"] = lst;


                }
                else
                {
                    lst = (List<User_ReleaseCodes>)Session["UREL"];
                    var pppp = lst.SingleOrDefault(a => a.ID == UR.ID);
                    if (pppp != default(User_ReleaseCodes))
                        lst.Remove(pppp);
                    var item = PRDB.user_ReleaseCodes.Find(UR.ID);
                    PRDB.user_ReleaseCodes.Remove(item);
                    PRDB.SaveChanges();

                }


            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }



        public ActionResult Lines_Read2([DataSourceRequest]DataSourceRequest request)
        {
            List<User_PurchasingGroups> lst = null;
            var list = new List<User_PurchasingGroups>();


            lst = null;
            if (Session["UPG"] == null)
            {
                lst = new List<User_PurchasingGroups>();
                Session["UPG"] = lst;

            }


            else
            {
                list = (List<User_PurchasingGroups>)Session["UPG"];
            }

            return Json(list.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Create2([DataSourceRequest]DataSourceRequest request, User_PurchasingGroups UR)
        {
            var AFDB = new AFOCDBContext();
            var  PRDB= new PRDBContext();

            var list = new List<User_PurchasingGroups>();
            var check = PRDB.user_PurchasingGroups.FirstOrDefault(x => x. PurchasingGroupID== UR.PurchasingGroupID);
            if (check == null)
            {

                UR.UserID = Session["UserId"].ToString();
                var re = AFDB.PurchasingGroups.Where(x => x.name == UR.PurchasingGroupID).FirstOrDefault();
                UR.PurchasingGroupDescription = re.Description;
                var u = new User_PurchasingGroups();
                if (((List<User_PurchasingGroups>)Session["UPG"]).Count == 0)//|| ((List<Line>)Session["PRLine"]).Count == 0)
                {
                    UR.ID = 1;
                    list.Add(UR);

                    Session["UPG"] = list;
                }
                else
                {
                    list = (List<User_PurchasingGroups>)Session["UPG"];
                    var first = list.OrderByDescending(e => e.ID).FirstOrDefault();
                    var id = (first != null) ? first.ID : 0;
                    UR.ID = id + 1;


                    list.Add(UR);


                    Session["UPG"] = list;
                }

                var UREL = new User_PurchasingGroups
                {

                    ID = UR.ID,
                    PurchasingGroupDescription = UR.PurchasingGroupDescription,
                    PurchasingGroupID = UR.PurchasingGroupID,
                    UserID = UR.UserID,
                };
                PRDB.user_PurchasingGroups.Add(UREL);
                PRDB.SaveChanges();

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Update2([DataSourceRequest]DataSourceRequest request, User_PurchasingGroups UR)
        {
            var PRDB = new PRDBContext();
            var AFDB = new AFOCDBContext();
            var lst = (List<User_PurchasingGroups>)Session["UPG"];
            var check = lst.First(x => x.PurchasingGroupID == UR.PurchasingGroupID);
            if (check == null)
            {
                UR.UserID = Session["UserId"].ToString();
                var re = AFDB.PurchasingGroups.Where(x => x.name == UR.PurchasingGroupID).FirstOrDefault();
                UR.PurchasingGroupDescription = re.Description;
                var i = lst.Where(x => x.ID == UR.ID).First();


                lst.Remove(i);
                lst.Add(UR);
                var UREL = new User_PurchasingGroups
                {

                    ID = UR.ID,
                    PurchasingGroupDescription = UR.PurchasingGroupDescription,
                    PurchasingGroupID = UR.PurchasingGroupID,
                    UserID = UR.UserID,
                };
                PRDB.user_PurchasingGroups.Attach(UREL);
                PRDB.Entry(UREL).State = EntityState.Modified;
                PRDB.SaveChanges();
                Session["UPG"] = lst;

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Destroy2([DataSourceRequest]DataSourceRequest request, User_PurchasingGroups UR)
        {



            var PRDB = new PRDBContext();



            if (UR != null)
            {
                List<User_PurchasingGroups> lst = null;
                if (Session["UPG"] == null)
                {
                    lst = new List<User_PurchasingGroups>();
                    Session["UPG"] = lst;


                }
                else
                {
                    lst = (List<User_PurchasingGroups>)Session["UPG"];
                    var pppp = lst.SingleOrDefault(a => a.ID == UR.ID);
                    if (pppp != default(User_PurchasingGroups))
                        lst.Remove(pppp);
                    var item = PRDB.user_PurchasingGroups.Find(UR.ID);
                    PRDB.user_PurchasingGroups.Remove(item);
                    PRDB.SaveChanges();

                }


            }
            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }




        public ActionResult Lines_Read3([DataSourceRequest]DataSourceRequest request)
        {
            List<User_MarketListPR> lst = null;
            var list = new List<User_MarketListPR>();


            lst = null;
            if (Session["UDT"] == null)
            {
                lst = new List<User_MarketListPR>();
                Session["UDT"] = lst;

            }


            else
            {
                list = (List<User_MarketListPR>)Session["UDT"];
            }

            return Json(list.ToDataSourceResult(request));
        }
    
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Create3([DataSourceRequest]DataSourceRequest request, User_MarketListPR UR)
        {
            var AFDB = new AFOCDBContext();
            var PRDB = new PRDBContext();

            var list = new List<User_MarketListPR>();
            var check = PRDB.user_MarketListPRs.FirstOrDefault(x => x.MasrketListPRID == UR.MasrketListPRID);
            if (check == null)
            {

                UR.UserID = Session["UserId"].ToString();
                var re = AFDB.MarketListPRs.Where(x => x.Type == UR.MasrketListPRID).FirstOrDefault();
                UR.MasrketListPRDescription = re.Description;
                var u = new User_MarketListPR();
                if (((List<User_MarketListPR>)Session["UDT"]).Count == 0)//|| ((List<Line>)Session["PRLine"]).Count == 0)
                {
                    UR.ID = 1;
                    list.Add(UR);

                    Session["UDT"] = list;
                }
                else
                {
                    list = (List<User_MarketListPR>)Session["UDT"];
                    var first = list.OrderByDescending(e => e.ID).FirstOrDefault();
                    var id = (first != null) ? first.ID : 0;
                    UR.ID = id + 1;


                    list.Add(UR);


                    Session["UDT"] = list;
                }

                var UREL = new User_MarketListPR
                {

                    ID = UR.ID,
                    MasrketListPRDescription = UR.MasrketListPRDescription,
                    MasrketListPRID = UR.MasrketListPRID,
                    UserID = UR.UserID,
                };
                PRDB.user_MarketListPRs.Add(UREL);
                PRDB.SaveChanges();

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Update3([DataSourceRequest]DataSourceRequest request, User_MarketListPR UR)
        {
            var PRDB = new PRDBContext();
            var AFDB = new AFOCDBContext();
            var lst = (List<User_MarketListPR>)Session["UDT"];
            var check = lst.First(x => x.MasrketListPRID == UR.MasrketListPRID);
            if (check == null)
            {
                UR.UserID = Session["UserId"].ToString();
                var re = AFDB.MarketListPRs.Where(x => x.Type == UR.MasrketListPRID).FirstOrDefault();
                UR.MasrketListPRDescription = re.Description;
                var i = lst.Where(x => x.ID == UR.ID).First();


                lst.Remove(i);
                lst.Add(UR);
                var UREL = new User_MarketListPR
                {

                    ID = UR.ID,
                    MasrketListPRDescription = UR.MasrketListPRDescription,
                    MasrketListPRID = UR.MasrketListPRID,
                    UserID = UR.UserID,
                };
                PRDB.user_MarketListPRs.Attach(UREL);
                PRDB.Entry(UREL).State = EntityState.Modified;
                PRDB.SaveChanges();
                Session["UDT"] = lst;

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Destroy3([DataSourceRequest]DataSourceRequest request, User_MarketListPR UR)
        {



            var PRDB = new PRDBContext();



            if (UR != null)
            {
                List<User_MarketListPR> lst = null;
                if (Session["UDT"] == null)
                {
                    lst = new List<User_MarketListPR>();
                    Session["UDT"] = lst;


                }
                else
                {
                    lst = (List<User_MarketListPR>)Session["UDT"];
                    var pppp = lst.SingleOrDefault(a => a.ID == UR.ID);
                    if (pppp != default(User_MarketListPR))
                        lst.Remove(pppp);
                    var item = PRDB.user_MarketListPRs.Find(UR.ID);
                    PRDB.user_MarketListPRs.Remove(item);
                    PRDB.SaveChanges();

                }


            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }



        public ActionResult Lines_Read4([DataSourceRequest]DataSourceRequest request)
        {
            List<User_Plant> lst = null;
            var list = new List<User_Plant>();


            lst = null;
            if (Session["UPL"] == null)
            {
                lst = new List<User_Plant>();
                Session["UPL"] = lst;

            }


            else
            {
                list = (List<User_Plant>)Session["UPL"];
            }

            return Json(list.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Create4([DataSourceRequest]DataSourceRequest request, User_Plant UR)
        {
            var AFDB = new AFOCDBContext();
            var PRDB = new PRDBContext();

            var list = new List<User_Plant>();
            var check = PRDB.user_Plants.FirstOrDefault(x => x.PlantID == UR.PlantID);
            if (check == null)
            {

                UR.UserID = Session["UserId"].ToString();
                var re = AFDB.Plants.Where(x => x.Type == UR.PlantID).FirstOrDefault();
                UR.PlantDescription = re.Description;
                var u = new User_Plant();
                if (((List<User_Plant>)Session["UPL"]).Count == 0)//|| ((List<Line>)Session["PRLine"]).Count == 0)
                {
                    UR.ID = 1;
                    list.Add(UR);

                    Session["UPL"] = list;
                }
                else
                {
                    list = (List<User_Plant>)Session["UPL"];
                    var first = list.OrderByDescending(e => e.ID).FirstOrDefault();
                    var id = (first != null) ? first.ID : 0;
                    UR.ID = id + 1;


                    list.Add(UR);


                    Session["UPL"] = list;
                }

                var UREL = new User_Plant
                {

                    ID = UR.ID,
                    PlantDescription = UR.PlantDescription,
                    PlantID = UR.PlantID,
                    UserID = UR.UserID,
                };
                PRDB.user_Plants.Add(UREL);
                PRDB.SaveChanges();

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }


            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Update4([DataSourceRequest]DataSourceRequest request, User_Plant UR)
        {
            var PRDB = new PRDBContext();
            var AFDB = new AFOCDBContext();
            var lst = (List<User_Plant>)Session["UPL"];
            var check = lst.First(x => x.PlantID == UR.PlantID);
            if (check == null)
            {
                UR.UserID = Session["UserId"].ToString();
                var re = AFDB.Plants.Where(x => x.Type == UR.PlantID).FirstOrDefault();
                UR.PlantDescription = re.Description;
                var i = lst.Where(x => x.ID == UR.ID).First();


                lst.Remove(i);
                lst.Add(UR);
                var UREL = new User_Plant
                {

                    ID = UR.ID,
                    PlantDescription = UR.PlantDescription,
                    PlantID = UR.PlantID,
                    UserID = UR.UserID,
                };
                PRDB.user_Plants.Attach(UREL);
                PRDB.Entry(UREL).State = EntityState.Modified;
                PRDB.SaveChanges();
                Session["UPL"] = lst;

            }
            else
            {
                return RedirectToAction("Lines_Destroy", UR);

            }


            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Lines_Destroy4([DataSourceRequest]DataSourceRequest request, User_Plant UR)
        {

            var PRDB = new PRDBContext();



            if (UR != null)
            {
                List<User_ReleaseCodes> lst = null;
                if (Session["UPL"] == null)
                {
                    lst = new List<User_ReleaseCodes>();
                    Session["UPL"] = lst;


                }
                else
                {
                    lst = (List<User_ReleaseCodes>)Session["UPL"];
                    var pppp = lst.SingleOrDefault(a => a.ID == UR.ID);
                    if (pppp != default(User_ReleaseCodes))
                        lst.Remove(pppp);
                    var item = PRDB.user_ReleaseCodes.Find(UR.ID);
                    PRDB.user_ReleaseCodes.Remove(item);
                    PRDB.SaveChanges();

                }


            }

            return Json(new[] { UR }.ToDataSourceResult(request, ModelState));
        }
    }
}
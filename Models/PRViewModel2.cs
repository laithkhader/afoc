﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class PRViewModel2
    {
        [Key]
        public string RepID { get; set; }
        public string DocID { get; set; }
        public Master Master { get; set; }
        public Line Line { get; set; }
        public string Plant_ID { get; set; }
        public string Plant_Description { get; set; }
        public string REL_DATE { get; set; }
        public string REL__Code { get; set; }

        public string PG_ID { get; set; }
        public string PG_Description { get; set; }


        public string status { get; set; }
        [Display(Name ="Approver 1")]
        public string REL_CD_TX1 { get; set; }
        [Display(Name = "Approver 2")]
        public string REL_CD_TX2 { get; set; }
        [Display(Name = "Approver 3")]
        public string REL_CD_TX3 { get; set; }
        [Display(Name = "Approver 4")]
        public string REL_CD_TX4 { get; set; }
        [Display(Name = "Approver 5")]
        public string REL_CD_TX5 { get; set; }
        [Display(Name = "Approver 6")]
        public string REL_CD_TX6 { get; set; }
        [Display(Name = "Approver 7")]
        public string REL_CD_TX7 { get; set; }
        [Display(Name = "Release Status")]
        public string REL_IND_TX { get; set; }
        public string MAT_GRP { get; set; }
        public bool Checked { get; set; }
        public PRViewModel2()
        {
            Master = new Master();
            Line = new Line();
            Checked = false;
        }
    }


}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class AccountAssignment

    {
        [Key]
        public string Type { get; set; }
        public string Description { get; set; }
    }
}
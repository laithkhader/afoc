﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class AFOCDBInitializer : CreateDatabaseIfNotExists<AFOCDBContext> //CreateDatabaseIfNotExists<AFOCDBContext>
    {
        protected override void Seed(AFOCDBContext context)
        {
            base.Seed(context);
            AFOCDBContext db = new AFOCDBContext();

            Plant p = new Plant();
            p.Type = "1000";
            p.Description = "AFOCH Club & Hotel";
            db.Plants.Add(p);

            p = new Plant();
            p.Type = "2000";
            p.Description = "AFOCH Strategic Business Outsi";
            db.Plants.Add(p);

            //CostCenter cost = new CostCenter();
            //cost.CostCenterID = "101001";
            //cost.Name = "Cost Center Test11";
            //cost.Description = "Abu-Dhabi";
            //cost.Status = "Act";
            //db.CostCenters.Add(cost);

            PurchasingGroup pur = new PurchasingGroup();
            pur.name = "101";
            pur.Description = "Executive Office";
            db.PurchasingGroups.Add(pur);

            pur.name = "102";
            pur.Description = "Executive Office";
            db.PurchasingGroups.Add(pur);
            //cost = new CostCenter();
            //cost.CostCenterID = "2";
            //cost.Name = "Cost Center Test22";
            //cost.Description = "Nablus";
            //cost.Status = "Act";
            //db.CostCenters.Add(cost);

            AccountAssignment acc = new AccountAssignment();
            acc.Type = "Y";
            acc.Description = "Y";
            db.AccountAssignments.Add(acc);

             acc = new AccountAssignment();
            acc.Type = "K";
            acc.Description = "K";
            db.AccountAssignments.Add(acc);

            MarketListPR m = new MarketListPR();
            
            m.Type = "ZMLP";
            m.Description = "Market List PR";
            db.MarketListPRs.Add(m);
            m = new MarketListPR();
            m.Type = "ZRLP";
            m.Description = "RemLoc MarketList PR";

            db.MarketListPRs.Add(m);

            //Material mat = new Material();
            //mat.MaterialID = "1";
            //mat.MaterialDesc = "White Bread";
            //mat.UnitPrice = decimal.Parse("2.5");
            //mat.UnitType ="KG";
            //mat.Status = "Act";
            //db.Materials.Add(mat);

            //mat = new Material();
            //mat.MaterialID = "2";
            //mat.MaterialDesc = "Turkey";
            //mat.UnitPrice = decimal.Parse("36");
            //mat.UnitType = "KG";
            //mat.Status = "Act";
            //db.Materials.Add(mat);

            //mat = new Material();
            //mat.MaterialID = "3";
            //mat.MaterialDesc = "Salmon";
            //mat.UnitPrice = decimal.Parse("65");
            //mat.UnitType = "KG";
            //mat.Status = "Act";
            //db.Materials.Add(mat);

            db.SaveChanges();
            try
            {
                AFOCDBContext db1 = new AFOCDBContext();

                Plant p1 = new Plant();

                p1.Type = "1000";
                db.Plants.Add(p1);
                //CostCenter cost = new CostCenter();
                //cost.CostCenterID = "101001";
                //cost.Name = "Cost Center Test11";
                //cost.Description = "Abu-Dhabi";
                //cost.Status = "Act";
                //db.CostCenters.Add(cost);

                PurchasingGroup pur1 = new PurchasingGroup();
                pur1.name = "101";
                
                db.PurchasingGroups.Add(pur1);
                //cost = new CostCenter();
                //cost.CostCenterID = "2";
                //cost.Name = "Cost Center Test22";
                //cost.Description = "Nablus";
                //cost.Status = "Act";
                //db.CostCenters.Add(cost);

                AccountAssignment acc1 = new AccountAssignment();
                acc1.Type = "Y";
                db.AccountAssignments.Add(acc1);

                acc1 = new AccountAssignment();
                acc1.Type = "K";
                db.AccountAssignments.Add(acc1);

                MarketListPR m1 = new MarketListPR();
                m1.Type = "Y";
                db.MarketListPRs.Add(m1);

                //Material mat1 = new Material();
                //mat1.MaterialID = "1";
                //mat1.MaterialDesc = "White Bread";
                //mat1.UnitPrice = decimal.Parse("2.5");
                //mat1.UnitType = "KG";
                //mat1.Status = "Act";
                //db1.Materials.Add(mat1);

                //mat1 = new Material();
                //mat1.MaterialID = "2";
                //mat1.MaterialDesc = "Turkey";
                //mat1.UnitPrice = decimal.Parse("36");
                //mat1.UnitType = "KG";
                //mat1.Status = "Act";
                //db1.Materials.Add(mat1);

                //mat1 = new Material();
                //mat1.MaterialID = "3";
                //mat1.MaterialDesc = "Salmon";
                //mat1.UnitPrice = decimal.Parse("65");
                //mat1.UnitType = "KG";
                //mat1.Status = "Act";
                //db1.Materials.Add(mat1);

                db1.SaveChanges();

            }
            catch { }
        }
    }
}
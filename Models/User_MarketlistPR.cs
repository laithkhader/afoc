﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class User_MarketListPR
    {
        public int ID { get; set; }

        public string UserID { get; set; }
        public string MasrketListPRID { get; set; }
        public string MasrketListPRDescription { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class User_Plant
    {
        public int ID { get; set; }

        public string UserID { get; set; }
        public string PlantID { get; set; }
        public string PlantDescription { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class User_PurchasingGroups
    {
        public int ID { get; set; }

        public string UserID { get; set; }
        public string PurchasingGroupID { get; set; }
        public string PurchasingGroupDescription { get; set; }

    }
}
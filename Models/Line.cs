﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class Line
    {
        [Display(Name = "D")]
        public string DELETE_IND { get; set; }
        public int ID { get; set; }

        [StringLength(40, ErrorMessage = "Max of 40 Character")]
     
        //[ForeignKey("Material")]
        [Required]
      
        public string MaterialID { get; set; }



        [StringLength(40, ErrorMessage = "Max of 40 Character")]
        [Display(Name = "Material Description")]
        public string MatDesc { get; set; }




        //[RegularExpression(@"^\d+\.\d{0,3}$", ErrorMessage = "Max of (13,3) ")]
        [Range(0.01, 9999999999999.999, ErrorMessage = "Min 0.01 ")]
        [Required]
        public decimal Quantity { get; set; }




        [StringLength(3, ErrorMessage = "Max of 3 Characters")]
        //[Required]
        public string Unit { get; set; }




        //[RegularExpression(@"^\d+\.\d{0,2}$", ErrorMessage = "Max of (11,2) ")]
        //[Range(0.11, 99999999999.99, ErrorMessage = "Max of (11,2) ")]
        //[Required]
        [Display(Name = "Unit Price")]
        public decimal UnitPrice { get; set; }


        //[RegularExpression(@"^\d+\.\d{0,2}$",ErrorMessage = "decemal format ")]
        //[Range(0, 999999999999999.99, ErrorMessage = "Max of (15,2) ")]
        //[Required]
        [Display(Name = "Total Price")]
        public decimal TotalPrice { get; set; }
   

        [StringLength(10, ErrorMessage = "Max of 10 Characters")]
        //[Required]
        [Display(Name = "Cost Center")]

        [Required]

        public string CostCenterID { get; set; }



        [StringLength(20, ErrorMessage = "Max of 20 Characters")]
        [Display(Name = "Cost Center Name")]
        public string CostCenterName { get; set; }



        [StringLength(40, ErrorMessage = "Max of 40 Characters")]
        [Display(Name = "Cost Center Description")]

        public string CostCenterDescription { get; set; }



        [StringLength(40)]
        [Display(Name = "Item Text")]

        public string ItemText { get; set; }

        [StringLength(500)]
        [Display(Name = "Material Po Text")]
        public string MaterialPoText { get; set; }


        [ForeignKey("Master")]
        public int MasterID { get; set; }


        public virtual Master Master { get; set; }



    }
}
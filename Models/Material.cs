﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AFOC_APP.Models
{
    public class Material
    {
        [Key]
        public string MaterialID { get; set; }

        public string MaterialDesc { get; set; }

        public decimal UnitPrice { get; set; }
        public string UnitType { get; set; }
        public string Status { get; set; }
    }
}